'use strict';

const hearts111=document.querySelector('.heart'); 
console.log(hearts111.prototype);//вывести прототип


const box=document.getElementById('box'), //Element! //по айдишникам,т.к id уникальны
      btns=document.getElementsByTagName('button'), //Elements!! //если искать, наапример по тегу, получим коллекцию
      circles=document.getElementsByClassName('circle'),
      wrapper=document.querySelector('.wrapper'), //позволяет получить только первый селектор, удобно, когда он единственый
      oneHeart=wrapper.querySelector('.heart'), //обращение может быть через wrapper, тогда будет искать только в нем
      hearts=wrapper.querySelectorAll('.heart');//абсолютно любой селектор
      // в этом методы появляется ForEach и прототип уже NodeList
      hearts.forEach(item=>{
        console.log(item);
    });
    
      
const btns2=document.getElementsByTagName('button')[1]; //получим 2ую кнопку, т.к указали индекс в массиве
//если в доме только одна кнопка все равно это коллекция
    

box.style.backgroundColor='blue'; //изменение свойств происходит через style и свойство CSS в кэмэлкейс
box.style.width='50px';

const num=900;
box.style.cssText=`background-color: yellow; width:${num}px;`; //присваивает эти стили

btns[1].style.borderRadius="100%"; 
//обратиться к псевдомассиву полностью не получиться, поэтому цикл
for(let i=0;i<hearts.length; i++){
    hearts[i].style.backgroundColor='blue';
}
//или
hearts.forEach(item=>{
item.style.backgroundColor='blue';
});


//создание 
const div=document.createElement('div'); //в коде он не появиться, просто присвоился переменной
const text=document.createTextNode('Тут был я'); //создание текстового узла

div.classList.add('black'); //назначаем этому элементу класс блэк, он уже есть у нас в стилях

//современные способы
document.body.append(div);//добавим див в конец родителя
wrapper.append(div); 
wrapper.appendChild(div);//то же
wrapper.prepend(div);
hearts[0].before(div); //добавим перед ним. Так же для after

//удаление
circles[0].remove();

//замена чего на что
hearts[0].replaceWith(circles[0]);

//раньше
wrapper.insertBefore(div,hearts[1]);
wrapper.removeChild(hearts[2]);
//wrapper.replaceChild(circles[0],hearts[0]);
//здесь приходилось и родителя искать

div.innerHTML='<h1>Hello 1!</h1>'; //вставить HTML структуру
div.textContent="Hello!"; //только для текста, используется для ответа от пользователя

div.insertAdjacentHTML('beforebegin','<h1>Hello 2!</h1>'); //перед началом дива
div.insertAdjacentHTML('beforeend','<h1>Hello 2!</h1>');// перед концом дива
div.insertAdjacentHTML('afterbegin','<h2>Hello 2!</h2>');// после начала дива
div.insertAdjacentHTML('afterend','<h2>Hello 2!</h2>');// после конца дива




