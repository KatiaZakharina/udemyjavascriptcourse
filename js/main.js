'use strict';

//------------------свойства и методы строк
const str='number Of Simbols';

//-----свойство
console.log(str.length); //свойство у строки одно
console.log("1234567"[3]); //4 //это не свойство, но нужно знать, что строка имеет индекс

//-----методы
console.log(str.toUpperCase());//метод, поэтому ()
console.log(str.toLowerCase());//метод, поэтому ()
console.log(str.indexOf('Simbols'));//найти кусочек строки и указать индекс начала
console.log(str.indexOf('Z'));//выведет -1, т.к нет такого значения
const logg="Hello my dear World";
console.log(logg.slice(6,13)); //вырезает из строки символы с по (не включая)
console.log(logg.slice(6)); //вырезаем с до конца
console.log(logg.slice(-5,-1)); //с конца
console.log(logg.substring(6,11)); //то же самое, но не поддерживает отрицательные и воспринимает их как 0
console.log(logg.substr(6,2));//вырезает, но с - и столько символов, сколько во втором числе
console.log(typeof(String('4')));//редко используется


//---------------- для чисел есть библиотека Math, которая есть прямо в браузере
let num=12.5;
console.log(Math.round(num));//округляем до ближайшего целого
const testCssSize="12.2px";
console.log(parseInt(testCssSize,10));//по идее преобразовывает к другой системе счисления
// но в реальности (используется для работы с размерми в CSS)
console.log(parseFloat(testCssSize)); //то же, но без округления
console.log(isNaN(testCssSize)); //true or false

console.log(typeof(Number('4'))); //редко используется, т.к есть +'4'

//----------------------------------------------для булиновых
//преобразование тивов
if(0){//=false
}
if(''){// =false //' '!=false
}
if(null){//=false
}
if(undefined){//=false
}
if(NaN){//=false

}

console.log(typeof(Boolean('4')));
console.log(typeof(!!('4')));//перобразует к булиновому, но почти не используется

//----------------------------------------------для объектов

const options={
    name: 'test',
    width: 1024,
    height: 1024,
    color:{
        border:'black',
        bg:'red'
    }
};

delete options.name; //вырежет указанное из объекта
for(let key in options){ //все свойста мы называем ключами. 
    //Этот метод выводит все свойства и их значения в красивом виде
     if (typeof(options[key])=='object'){
            for(let i in options[key]){
                console.log(`Свойство ${i} имеет значение ${options[key][i]}`);
            }  
        }
     else {
         console.log(`Свойство ${key} имеет значение ${options[key]}`);
        }
    }


    console.log(Object.keys(options));//этот метод создает массив из ключей, при выводе ключи
    console.log(Object.keys(options).length);//создает массив из ключей, а у массива есть длина

//Методы создания поверхностных копий:
    /* - for in
    - Object.assign({}, obj); - метод позволяет влить второе в первое
    //Совмещение двух объектов
    - let arr1=arr2.slice();
 */

    // Spread    (появился в ES6 и ES8 для массивов и объектов соответственно)
    //spread совмещение двух массивов
    const video=["youtube","vimeo","rutube"],
                blog=["wordpress","lifejournal"],
                internet=[...video,...blog,"vk","pinterest"];
    console.log(internet);
    
    /*[
      'youtube',
      'vimeo',
      'rutube',
      'wordpress',
      'lifejournal',
      'vk',
      'pinterest'
    ] */
    
    //spread передача элементов массива в качестве аргументов функции
    function myFunction(x, y, z) { }
    var args = [0, 1, 2];
    myFunction(...args);
    
    //spread поверхностное копирование
    const obj={
    a:'a',
    b:'b'
    };
    const newObj={...obj}; // jshinte может ругаться т.к jsversion в файле стоит на ES9 


//проверка на существование свойства, можно было бы и age===undefined, но если кто-то вручную создал св-во и присвоил undefined, то неверно
let user = { name: "John", age: 30 };

alert( "age" in user ); // true, user.age существует
alert( "blabla" in user ); // false, user.blabla не существует


    //-------------------------------------массивы

    const arr =[1,2,3,6,8];
arr.pop(); //удаляет последний элемент из массива
arr.push(19); //добавляет в конце элемент

arr.shift(); //удаляет первый элемент из массива
arr.unshift(5); //добавляет в начале элемент
//----!---нюанс в том, что все последующие элементы меняют номер, и это долго

//перебор элементов массива
for(let i=0; i<arr.length; i++){
console.log(arr[i]);
}


for (let value of arr){ //так же как и в фор ин случайное слово для элементов
    console.log(value);

}   

arr.forEach(function(item,i,arr){//callback 3 аргумента 1-эл-т 2-номер 3-массив
    //сначала выполниться forEach, а после коллбэки
    console.log(`${i}:${item} внутри массива ${arr}`);
    }
    );
 //for of поддерживает break и continue

//трансформирующие методы перебора
/* .map
.filter
.every/some */


const strochka=prompt("?",""); // метод позволяет из строки с разделителями сост массив
const products=strochka.split(", "); //в скобках разделитель в строке
//сортировка сортирует посимвольно, т.е сначала по перому символу, затем по второму...
products.sort();
console.log(products);

//обратный метод формирует строку из массива
const newString=products.join("/// "); //в скобках разделитель через который выведет эл-ты
console.log(newString);

//правильная сортировка чисел
const arr1 = [2,5,3,6,8,5];
arr1.sort(compaireNum);
function compaireNum(a,b){
    return a-b;
    }
    //это связано с быстрой сортировкой
console.log(arr1);